package com.targetclock;

import java.io.*;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Seriabilidade {
    private File file;
    private File arestas;
    private List<Node> nodes = new ArrayList<Node>();

    public Integer[] getArrayNodes() {
        List<Integer> lNode = new ArrayList<Integer>();
        for (Node node : nodes) {
            lNode.add((int)node.getId());
        }
        return (Integer[]) lNode.toArray();
    }


    public Seriabilidade(String pahtInput) {
        file = new File(pahtInput);
    }

    public void makeValues() {
        try (BufferedReader br = new BufferedReader(new FileReader(file))){
            String linha = br.readLine();

            while (linha != null) {
                String[] aLinha = linha.split(" ");
                obtainNodes(Integer.valueOf(aLinha[1]));
//                gravaAresta(aLinha);
                linha = br.readLine();
            }
            closeAresta();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void obtainNodes(Integer Linha) {
        if (!this.nodes.contains(Linha)) {
            this.nodes.add(new Node(Linha));
        }
    }

    public List<Node> getNodes() {
        return this.nodes;
    }

    private void closeAresta() {
    }

    public void arestaTiwTjr(){

    }

    public void gravaAresta(String[] aresta) throws IOException {
        this.arestas = new File("./inputs/arestas.in");

        // if file doesnt exists, then create it
        if (!arestas.exists()) {
            arestas.createNewFile();
        }

        BufferedWriter bw = new BufferedWriter(new FileWriter(arestas.getAbsoluteFile(), true));
        if (aresta[2].equals("R") || aresta[2].equals("W")  ) {
            bw.write(Arrays.toString(aresta)+"\r");
            System.out.println(aresta[1]+","+aresta[2]+","+aresta[3]);
        }
        bw.close();
    }
}
