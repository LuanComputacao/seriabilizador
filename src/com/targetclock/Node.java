package com.targetclock;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luan on 07/04/15.
 */
public class Node {
    private List<Integer> arestas = new ArrayList<Integer>();
    private int id;


    /**
     * Id do nodo(transação)
     * @param id
     */
    public Node(int id) {
        this.id = id;
    }

    /*------------------------------------------------------------------------------------
    | Setters And Getters
    ------------------------------------------------------------------------------------*/

    /*------------------------
    | Arestas
    ------------------------*/
    public List<Integer> getArestas() {
        return arestas;
    }

    public void setArestas(List<Integer> arestas) {
        this.arestas = arestas;
    }

    /*------------------------
    | Id
    ------------------------*/

    /**
     *
     * @return
     */
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    /*------------------------------------------------------------------------------------
    | Others methods
    ------------------------------------------------------------------------------------*/


    /**
     * Adiciona um alvo de aresta
     * @param node
     */
    public void addAresta(int node){
        this.arestas.add(node);
    }

    /**
     * Verifica se existe o id passado como ponta de aresta
     * @param id
     * @return
     */
    public boolean hasAresta(int id) {
        return this.arestas.contains(id);
    }
}
