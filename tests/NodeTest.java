import com.targetclock.Node;

import org.junit.*;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;


/**
 * Created by luan on 07/04/15.
 */
public class NodeTest {
    private Node node;

    @Before
    public void inicializa(){
        this.node = new Node(1);
    }

    @Test
    public void retornaId(){
        assertEquals(1, node.getId());
    }

    @Test
    public void insereAresta(){
        List<Integer> testeAresta = new ArrayList<Integer>(){{add(2);}};
        node.addAresta(2);
        assertEquals(testeAresta, node.getArestas());
    }
}
