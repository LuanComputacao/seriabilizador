import com.targetclock.Node;

import java.util.ArrayList;

/**
 * Created by luan.santana on 25/03/2015.
 */
public class SeriabilidadeTestPrint {

    public static void main(String[] args) {
        ArrayList<Node> nodes = new ArrayList<Node>() {{
            add(new Node(1));
            add(new Node(2));
            add(new Node(3));
            add(new Node(4));
        }};
        for (Node node : nodes)
            System.out.println(node.getId());
    }
}
