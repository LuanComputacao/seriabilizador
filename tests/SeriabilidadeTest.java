import com.targetclock.Node;
import com.targetclock.Seriabilidade;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class SeriabilidadeTest {

    private Seriabilidade seriabilidade;

    @Before
    public void inicializa() {
        this.seriabilidade = new Seriabilidade("./inputs/teste.in");
        seriabilidade.makeValues();
    }

    @Test
    public void deveLerERetornarUmaEntradaVariável() {

    }

    @Test
    public void deveRetornarAQuantidadeDeNos() {
        Integer[] esperado = {0, 1, 2, 3};
        Integer[] aNodes = seriabilidade.getArrayNodes();

        assertArrayEquals(esperado, aNodes);
    }



    @Test
    public void deveRetornarAsArestasTiwTjr(){

    }

//    @Test
//    public void deveLerERetornarUmaEntradaVariávelGrande() {
//        seriabilidade.setFile("./inputs/teste_big.in");
//        seriabilidade.makeValues();
//    }

}
